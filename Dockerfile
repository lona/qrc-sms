FROM node:10

# Create app directory
WORKDIR /app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm install --only=production

# Bundle app source
COPY . .

RUN npm run build

FROM node:10
WORKDIR /app

COPY --from=0 /app/build /app/build
COPY --from=0 /app/package.json /app/

RUN npm install --only=production

EXPOSE 3000
CMD [ "npm", "start" ]
