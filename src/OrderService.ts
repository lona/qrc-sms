import { Order } from "./OrderModel";
import { SmsService } from "./SmsService";
import { orderParseError, newOrderNotification } from "./msgs";
import { logger } from "./logger";

export class OrderService {

    constructor(private _recipients: string[],
        private _smsService: SmsService) {}

    async handleRecvOrder (orderPayload: any) {
        try {

            if(orderPayload.webhook_id) {
                logger.debug({message: "received webhook ping", payload: orderPayload})
                return;
            }

            logger.info(`Received order ${orderPayload.id}`);
            logger.debug(orderPayload);

            let order = Order.FromWooCommerceHook(orderPayload);

            this._smsService.send(this._recipients,
                newOrderNotification(order));

        } catch (e) {
            logger.error({message: e.message, stack: e.stack});
            this._smsService.send(this._recipients, orderParseError());
        }
    }
}
