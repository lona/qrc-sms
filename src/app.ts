import { SmsService } from "./SmsService";

import Koa from 'koa';

import Router from 'koa-router';
import { OrderService } from "./OrderService";

import bodyParser from 'koa-bodyparser';
import { Order } from "./OrderModel";
import { logger } from "./logger";

export function AppFactory (orderService: OrderService) {

  const app = new Koa();
  var router = new Router();

  router.post('/api/order', (ctx) => {

      ctx.status = 202;

      orderService.handleRecvOrder(ctx.request.body);
  });

  app
    .use(bodyParser())
    .use(router.routes())
    .use(router.allowedMethods());

  return app;
}

