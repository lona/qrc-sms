import moment, { Moment } from 'moment';
import { parseWooDate } from './util';

interface OrderDto {
  custom: undefined
  number: string
  meta_data: Array<{key: string, value: string}>
}

interface CustomOrderDto {
  number: 'FORM'
  name: string
  phone: string
}

type OrderDtoTypes = OrderDto | CustomOrderDto | {};

export class Order {

    constructor(
      public id: string,

      public selectedDeliveryTime: Moment,
      public orderPlacedDateTime: Moment = moment(),

      public isCustom: boolean = false,
      public customerName: string = "",
      public customerPhone: string = "") {}


    get shortId (): string {
      return this.id.substr(-2);
    }

    static isCustomOrderDto (obj: OrderDtoTypes): obj is CustomOrderDto {
      return (obj as CustomOrderDto).number === 'FORM';
    }

    static FromWooCommerceHook (dto: OrderDtoTypes) {

      if (Order.isCustomOrderDto(dto)) {

        return new Order("", moment(), moment(), true, dto.name, dto.phone);

      } else {

        let orderDto = dto as OrderDto;

        let metaDatum = orderDto.meta_data
            .find((d: any) => d.key === "_openinghours_time");


        let date = metaDatum && metaDatum.value ? metaDatum.value: ''

        return new Order(orderDto.number, parseWooDate(date));

      }

    }
}
