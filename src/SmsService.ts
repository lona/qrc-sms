import EventEmitter from 'events';

import {SENDING_NUMBER} from './config';
import {nexmo} from './vendor/NexmoSingleton';
import { logger } from './logger';

export class SmsService {

  private _emitter: EventEmitter;

  constructor () {
    this._emitter = new EventEmitter();
  }

  send (recipients: string[], message: string) {

    const requests = recipients.map(recipient => {

      return new Promise((resolve, reject) => {

        logger.info(`Sending message to ${recipient}: ${message}`)

        nexmo.message.sendSms(
          SENDING_NUMBER, recipient, message,

          (error: any, response: any) => {

            if (error) {
              logger.debug(`nexmo failure ${error}`)
              return reject(error);
            }

            logger.debug(`nexmo success ${response}`)
            return resolve(response);
          }
        );

      });

    });

    return Promise.all(requests);
  }

  // registerReciever (handler: MsgRecieverDelegate) {

  //   this._emitter.on(RECV_EVENT, handler);

  //   return () => this._emitter.removeListener(RECV_EVENT, handler);
  // }

  // handleMessageReceipt (number: string, msg: string) {
  //   this._emitter.emit(RECV_EVENT, number, msg);
  // }

}
