import Nexmo from 'nexmo';

import {NEXMO_API_KEY, NEXMO_API_SECRET} from '../config';

export const nexmo = new Nexmo({
  apiKey: NEXMO_API_KEY,
  apiSecret: NEXMO_API_SECRET
})
