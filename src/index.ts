import {AppFactory} from './app';

import {SmsService} from './SmsService'
import { OrderService } from './OrderService';
import { RECIPIENTS } from './config';
import { logger } from './logger';

const PORT = 3000;

const recipients = (RECIPIENTS || '').split(',');

let smsService = new SmsService();
let orderService = new OrderService(recipients, smsService);

let app = AppFactory(orderService);

app.listen(PORT, '', function () {
  logger.info(`Listening on ${PORT}`)
});

process.on('SIGINT', function() {
  process.exit();
});
