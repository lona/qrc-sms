import { SmsService } from "../SmsService";

import {nexmo} from '../vendor/NexmoSingleton';

import {SENDING_NUMBER} from '../config';

jest.mock('../vendor/NexmoSingleton')
// jest.unmock('nexmo')

describe('SmsService', () => {

  describe('when sending a text', () => {
    beforeEach(async () => {
        let sms = new SmsService();

        let r = await sms.send([
            "00000000000",
            "00000000001"
        ], "SENT MSG!!");
    });

    it('should sent a message for each number', async () => {
        expect(nexmo.message.sendSms)
            .toHaveBeenCalledTimes(2);
    });

    it('should have sent a message to each specified number', () => {
        let recipients = nexmo.message.sendSms.mock.calls.map((call: string[]) => {
            return call[1]
        });

        expect(recipients).toContain("00000000000")
        expect(recipients).toContain("00000000001")
    });

    it('should send the sms from the same configured number and message', () => {
        nexmo.message.sendSms.mock.calls
        .forEach((call: string[]) => {
            let [sending, recipient, msg] = call;

            expect(sending).toEqual(SENDING_NUMBER);
            expect(msg).toEqual("SENT MSG!!");
        });
    });

  });

});
