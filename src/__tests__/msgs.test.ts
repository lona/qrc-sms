import * as msgs from '../msgs';
import { Order } from '../OrderModel';
import moment = require('moment');


describe('msgs', () => {

  it('should format custom orders', () => {

    let r = msgs.newOrderNotification(new Order(
      "",
      moment('2001-01-06'),
      moment('2001-01-06'),
      true,
      "CUSTOMER1",
      "232323"));

    expect(r).toEqual("Custom order placed by CUSTOMER1 232323");
  });

  it('should format standard orders', () => {

    let r = msgs.newOrderNotification(new Order(
      "220034",
      moment('2001-01-06')));

    expect(r).toEqual("Order 34 scheduled at 12:00am (1/6)");
  });

});
