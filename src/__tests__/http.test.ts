import {AppFactory} from '../app'
import { Server } from 'http';

import axios from 'axios';
import { OrderService } from '../OrderService';

jest.mock('../OrderService');

describe('App', async () => {
  let server: Server;
  let app;
  let orderService: OrderService;

  beforeEach(async () => {

    orderService = {handleRecvOrder: jest.fn()} as any as OrderService;

    app = AppFactory(orderService);

    server = app.listen(3001)
  });

  afterEach(() => {
    server.close();
  });

  it('should create an order object and pass it to the order service', async () => {

    const testJson = {anything: 'works'};


    await axios.post("http://localhost:3001/api/order", testJson);

    expect(orderService.handleRecvOrder).toHaveBeenCalledWith(testJson);

    // await axios.post("http://localhost:3001/api/order", {
    //   "number": "14311",
    //   "meta_data": [
    //     {
    //       "id": 526599,
    //       "key": "_openinghours_time",
    //       "value": "2019/01/17 10:45 am"
    //     }
    //   ],
    // });

    // expect(orderService.handleRecvOrder).toHaveBeenCalledWith({
    //   deliveryTime: "01/17 10:45 am", "id": "14311"
    // });

  });
});
