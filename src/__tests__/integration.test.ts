import {AppFactory} from '../app'
import { Server } from 'http';

import axios from 'axios';
import { OrderService } from '../OrderService';
import { SmsService } from '../SmsService';

jest.mock('../SmsService');

describe('App', async () => {

  let server: Server;
  let app;
  let smsService: SmsService;
  const testRecipients = ['16305559345']
  beforeEach(async () => {

    smsService = new SmsService();
    jest.spyOn(smsService, 'send')
      .mockImplementation(() => {});

    let orderService = new OrderService(testRecipients, smsService);

    app = AppFactory(orderService);

    server = app.listen(3001)
  });

  afterEach(() => {
    server.close();
  });

  it('should send an sms when there is an error', async () => {

    const testJson = {anything: 'works'};

    await axios.post("http://localhost:3001/api/order", testJson);

    expect(smsService.send).toHaveBeenCalledWith(
      testRecipients,
      "FYI I may have received an order but I glitched out."
    );

  });

  it('should ignore webhooks', async () => {
    const testJson = {"webhook_id":"3"};

    await axios.post("http://localhost:3001/api/order", testJson);

    expect(smsService.send).not.toHaveBeenCalled()
  });

  it('should send the order text when receiving an order', async () => {


    await axios.post("http://localhost:3001/api/order", {
      "number": "14311",
      "meta_data": [
        {
          "id": 526599,
          "key": "_openinghours_time",
          "value": "2019/01/17 10:45 am"
        }
      ],
    });

    expect(smsService.send).toHaveBeenCalledWith(
      testRecipients,
      "Order 11 scheduled at 10:45am (1/17)"
    );

  });

  it('should send sms with a custom order', async () => {


    await axios.post("http://localhost:3001/api/order", {
      "number": "FORM",
      "name": "Mir",
      "phone": "12345"
    });

    expect(smsService.send).toHaveBeenCalledWith(
      testRecipients,
      "Custom order placed by Mir 12345"
    );

  });


});
