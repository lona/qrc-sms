import { Order } from "../OrderModel";
import moment = require("moment");

describe('OrderModel', () => {

  beforeEach(() => {
    jest.spyOn(Date, 'now')
      .mockImplementation(jest.fn(() =>
        new Date(Date.UTC(2017, 0, 1, 6)).valueOf()))
  });

  it('should go', () => {
    let order = Order.FromWooCommerceHook({
      "number": "FORM",
      "meta_data": [{
        key:'asdf',
        value: '2019\/01\/17 1:27 pm'
      }]
    });

    expect(order.id).toEqual("");
    expect(order.selectedDeliveryTime.format())
      .toEqual(moment('2017-01-01T00:00:00').format())
  });

  it('should work', () => {

    let order = Order.FromWooCommerceHook({
      "number": "FORM",
      "name": "vince",
      "phone": "0001112223233",
    });

    expect(order.id).toEqual("")
    expect(order.customerName).toEqual("vince")
    expect(order.customerPhone).toEqual("0001112223233")
  });

});
