import {parseWooDate, formatShortDate} from '../util';
import moment = require('moment-timezone');

describe('conv', () => {

  test('date parsing from woocommerce', () => {

    let date = parseWooDate('2019\/01\/17 1:27 pm');
    expect(date.format()).toEqual('2019-01-17T13:27:00-06:00');

  });

  test('formatting for short date', () => {

    let result = formatShortDate(moment("2019-01-17T08:27:00-00:00"));

    expect(result).toEqual('2:27am (1/17)');
  });

});
