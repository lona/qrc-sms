import { OrderService } from "../OrderService";
import { Order } from "../OrderModel";

import {SmsService} from '../SmsService';
import {orderParseError, newOrderNotification} from '../msgs';
import moment = require("moment");

jest.mock('../SmsService');

describe('OrderService', () => {

  let smsService = new SmsService();
  let recipients = ["123", "456"];

  let orderService: OrderService;

  beforeEach(() => {


    orderService = new OrderService(recipients, smsService);

  });

  describe('when an order is received', () => {

    let incomingOrder: any;

    describe('always', () => {
      beforeEach(() => {

        incomingOrder = {any: 'object'};

        jest.spyOn(Order, 'FromWooCommerceHook')
          .mockImplementation(() => {
            return new Order('12312', moment());
          });

        orderService.handleRecvOrder(incomingOrder);
      });

      it('should parse the order json into the Order Model', () => {
        expect(Order.FromWooCommerceHook).toHaveBeenCalledWith(incomingOrder);
      });
    });


    describe('when the Order Model cant be parsed', () => {
      beforeEach(async () => {

        incomingOrder = {any: 'object'};

        jest.spyOn(Order, 'FromWooCommerceHook')
          .mockImplementation(() => {
            throw new Error('some error');
          });

        await orderService.handleRecvOrder(incomingOrder);
      });

      it('should send an sms anyways', () => {

        expect(smsService.send)
          .toHaveBeenCalledTimes(1)

        expect(smsService.send)
          .toHaveBeenCalledWith(recipients,
            orderParseError())
      });
    });

    describe('when the order payload is parsed successfully', () => {

      let order: Order;

      beforeEach(async () => {

        incomingOrder = {any: 'object'};
        order = new Order("12345", moment());

        jest.spyOn(Order, 'FromWooCommerceHook')
          .mockImplementation(() => {
            return order;
          });

        await orderService.handleRecvOrder(incomingOrder);

      });

      it('should send the new order notification', () => {
        expect(smsService.send)
          .toHaveBeenCalledWith(recipients, newOrderNotification(order))
      });

    });

  });

});
