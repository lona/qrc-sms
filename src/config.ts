import { logger } from "./logger";

export const {
  NEXMO_API_KEY,
  NEXMO_API_SECRET,
  SENDING_NUMBER,
  RECIPIENTS
} = process.env;


logger.debug({NEXMO_API_KEY,
  SENDING_NUMBER,
  RECIPIENTS})
