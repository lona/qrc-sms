import { Order } from "./OrderModel";
import { formatShortDate } from "./util";

export const orderParseError = () =>
  "FYI I may have received an order but I glitched out.";

export const newOrderNotification = (order: Order) =>{
  if(order.isCustom) {
    return `Custom order placed by ${order.customerName} ${order.customerPhone}`;
  } else {
    return `Order ${order.shortId} scheduled at ${formatShortDate(order.selectedDeliveryTime)}`;
  }
}
