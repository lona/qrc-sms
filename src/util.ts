import moment, { Moment } from 'moment-timezone';

export function parseWooDate (datestr: string): Moment {
  return moment.tz(
    datestr,
    'YYYY\/MM\/DD hh:mm a',
    "America/Chicago");
}

export function formatShortDate (m: Moment) {
  return m.tz("America/Chicago").format('h:mma (M/D)')
}
